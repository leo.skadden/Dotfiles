" Dein
if &compatible
  set nocompatible
endif
set runtimepath+=/home/alkenes/.local/share/dein/repos/github.com/Shougo/dein.vim

if dein#load_state("/home/alkenes/.local/share/nvim/plugged")
  call dein#begin("/home/alkenes/.local/share/nvim/plugged")

  call dein#add("/home/alkenes/.local/share/dein/repos/github.com/Shougo/dein.vim")
  call dein#add('Shougo/deoplete.nvim')
  if !has('nvim')
    call dein#add('roxma/nvim-yarp')
    call dein#add('roxma/vim-hug-neovim-rpc')
  endif

  call dein#end()
  call dein#save_state()
endif

filetype plugin indent on
syntax enable
